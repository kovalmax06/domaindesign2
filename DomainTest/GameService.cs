﻿using System;
using DomainTest;
using Test_Design.Interfaces;

namespace Test_Design
{
    public class GameService
    {
        private readonly IDeliveryService _deliveryService;
        private readonly IReturnService _returnService;
        private readonly IGiftService _giftService;
        private readonly IReviewService _reviewService;
        private readonly INewsService _newsService;
        private readonly ICashService _cashService;

        public GameService(IDeliveryService deliveryService, IReturnService returnService, 
            IGiftService giftService, IReviewService reviewService, 
            INewsService newsService, ICashService cashService)
        {
            _deliveryService = deliveryService;
            _returnService = returnService;
            _giftService = giftService;
            _reviewService = reviewService;
            _newsService = newsService;
            _cashService = cashService;
        }

        public void BuyGame(Player player, Game game)
        {
            player.BuyGame(game);
            _deliveryService.DeliverGame(game, player);
        }

        public void ReturnGame(Purchase purchase)
        {
            purchase.Player.ReturnGame(purchase);
            _returnService.ReturnGame(purchase);
        }

        public void GiftGame(Player startPlayer, Player endPlayer, Game game)
        {
            startPlayer.GiftGame(endPlayer, game);
            _giftService.GiftGame(endPlayer, game);
            _deliveryService.DeliverGame(game, endPlayer);
        }

        public void AddReview(Player player, string message, Game game)
        {
            player.WriteReview(message, game);
            _reviewService.AddReview(player, message, game);
        }

        public void AddNews(GameCompany gameCompany, string message, Game game)
        {
            gameCompany.WriteNews(message, game);
            _newsService.AddNews(gameCompany, message, game);
        }

        public void ReplenishMoney(Player player, decimal sum)
        {
            player.ReplenishMoney(sum);
            _cashService.ReplenishMoney(player, sum);
        }

        public void WithdrawMoney(Player player, decimal sum)
        {
            player.WithdrawMoney(sum);
            _cashService.WithdrawMoney(player, sum);
        }
    }
}