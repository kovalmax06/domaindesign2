﻿using System;
using System.Collections.Generic;
using System.Security.Authentication.ExtendedProtection.Configuration;
using DomainTest;

namespace Test_Design
{
    public class GameCompany
    {
        public string Name { get; set; }
        public List<Game> Games = new List<Game>();

        public GameCompany(string name)
        {            
            Name = name;
        }

        public void WriteNews(string message, Game game)
        {
            if (Games.Contains(game))
            {
                game.News.Add(new News(this, message, game));
            }
            else
            {
                throw new InvalidOperationException("Игровая компания не может добавить новость к чужой игре");
            }
        }
    }
}