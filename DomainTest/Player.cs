﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using Test_Design;

namespace DomainTest
{
    public class Player
    {
        public int Id { get; set; }
        public List<Game> Games { get; private set; }
        public decimal Balance { get; private set; }
        
        public Player()
        {
            Games = new List<Game>();
            Balance = 0;
        }

        public void BuyGame(Game game)
        {
            CheckBuyConditions(this, game);
            Games.Add(game);
            Balance -= game.Price;
            new Purchase(this, game);
        }

        public void AddGame(Game game)
        {
            Games.Add(game);
        }

        public void ReturnGame(Purchase purchase)
        {
            if (!Games.Contains(purchase.Game))
            {
                throw new InvalidOperationException("Нельзя вернуть игру, которой у вас нет");
            }

            if ((DateTime.Now.Date - purchase.CurrentTime).TotalDays > purchase.Game.WarrantyDays)
            {
                throw new InvalidOperationException("Невозможно вернуть игру, потому что гарантийный срок возврата истек");
            }

            Games.Remove(purchase.Game);
            ReplenishMoney(purchase.Game.Price);
        }

        public void GiftGame(Player endPlayer, Game game)
        {
            CheckBuyConditions(endPlayer, game);
            endPlayer.Games.Add(game);
            Balance -= game.Price;
            new Purchase(this, game);
            new Gift(this, endPlayer, game);
        }
        
        
        public void WriteReview(string message, Game game)
        {
            if (Games.Contains(game))
            {
                game.Reviews.Add(new Review(this, message, game));
            }
            else throw new InvalidOperationException("Game not exists in player's Games");
        }


        public void ReplenishMoney(decimal sum)
        {
            Balance += sum;
        }

        public void WithdrawMoney(decimal sum)
        {
            if (sum > Balance)
            {
                throw new InvalidOperationException("Сумма вывода денег превышает текущий баланс");
            }

            Balance -= sum;
        }
        

        private void CheckBuyConditions(Player player, Game game)
        {
            if (player.Games.Contains(game))
            {
                throw new InvalidOperationException("Game already exist in User Account");
            }

            if (Balance <= game.Price)
            {
                throw new InvalidOperationException("Balance less then Game Price");
            }
        }
    }
}