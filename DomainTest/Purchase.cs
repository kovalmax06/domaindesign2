﻿using System;

namespace DomainTest
{
    public class Purchase
    {
        public int Id { get; private set; }
        public Player Player { get; set; }
        public Game Game { get; set; }
       public DateTime CurrentTime { get; set; }

        public Purchase(Player player, Game game)
        {
            Player = player;
            Game = game;
            CurrentTime = DateTime.Now;
        }
    }
}