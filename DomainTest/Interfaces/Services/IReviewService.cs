﻿using DomainTest;

namespace Test_Design.Interfaces
{
    public interface IReviewService
    {
        void AddReview(Player player, string message, Game game);
    }
}