﻿using DomainTest;

namespace Test_Design.Interfaces
{
    public interface INewsService
    {
        void AddNews(GameCompany gameCompany, string message, Game game);
    }
}