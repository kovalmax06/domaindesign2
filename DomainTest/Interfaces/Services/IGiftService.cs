﻿using DomainTest;

namespace Test_Design.Interfaces
{
    public interface IGiftService
    {
        void GiftGame(Player endPlayer, Game game);
    }
}