﻿using DomainTest;

namespace Test_Design.Interfaces
{
    public interface ICashService
    {
        void ReplenishMoney(Player player, decimal sum);
        void WithdrawMoney(Player player, decimal sum);
    }
}