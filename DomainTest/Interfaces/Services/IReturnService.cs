﻿using DomainTest;

namespace Test_Design.Interfaces
{
    public interface IReturnService
    {
        void ReturnGame(Purchase purchase);
    }
}