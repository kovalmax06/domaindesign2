﻿using DomainTest;

namespace Test_Design.Interfaces
{
    public interface IGameRepository
    {
        Game Get(int id);
    }
}