﻿using DomainTest;

namespace Test_Design.Interfaces
{
    public interface IPlayerRepository
    {
        Player Get(int id);
    }
}