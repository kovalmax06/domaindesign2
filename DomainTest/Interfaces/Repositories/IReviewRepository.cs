﻿namespace Test_Design.Interfaces
{
    public interface IReviewRepository
    {
        Review Get(int id);
    }
}