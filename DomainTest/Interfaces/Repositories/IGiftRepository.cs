﻿namespace Test_Design.Interfaces
{
    public interface IGiftRepository
    {
        Gift Get(int id);
    }
}