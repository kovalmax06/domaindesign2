﻿using DomainTest;

namespace Test_Design.Interfaces
{
    public interface IPurchaseRepository
    {
        Purchase Get(int id);
    }
}