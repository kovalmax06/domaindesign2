﻿namespace Test_Design.Interfaces
{
    public interface INewsRepository
    {
        News Get(int id);
    }
}