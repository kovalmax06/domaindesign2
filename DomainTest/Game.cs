﻿using System.Collections.Generic;
using Test_Design;

namespace DomainTest
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LinkToFile { get; set; }
        public decimal Price { get; set; }
        public int WarrantyDays { get; set; }
        public List<Review> Reviews { get; private set; }
        public List<News> News { get; private set; }
        public List<Player> Players { get; private set; }

        public Game(string name, string linkToFile, decimal price, int warrantyDays)
        {
            Name = name;
            LinkToFile = linkToFile;
            Price = price;
            WarrantyDays = warrantyDays;
            Reviews = new List<Review>();
            News = new List<News>();
            Players = new List<Player>();
        }
    }
}