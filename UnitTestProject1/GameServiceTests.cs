﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Test_Design.Interfaces;
using Test_Design;
using DomainTest;

namespace UnitTestProject1
{
    [TestClass]
    public class GameServiceTests
    {
        [TestMethod]
        public void Buy_OneGame_OneGameAddInUserGames()
        {
            var deliveryService = new Mock<IDeliveryService>();
            var returnService = new Mock<IReturnService>();
            var giftService = new Mock<IGiftService>();
            var reviewService = new Mock<IReviewService>();
            var newsService = new Mock<INewsService>();
            var cashService = new Mock<ICashService>();
            
            Player player = new Player() { Id = 1 };
            player.ReplenishMoney(800);
            Game game = new Game("Far Cry 5", "Link", 100, 5) { Id = 1 };
            
            GameService gameService = new GameService(deliveryService.Object, returnService.Object, giftService.Object, reviewService.Object, newsService.Object, cashService.Object);
            gameService.BuyGame(player, game);

            deliveryService.Verify(x => x.DeliverGame(game, player), Times.Once);
            Assert.AreEqual(700, player.Balance);
        }

        [TestMethod]
        public void Gift_OneGame_OneGameAdd()
        {
            var deliveryService = new Mock<IDeliveryService>();
            var returnService = new Mock<IReturnService>();
            var giftService = new Mock<IGiftService>();
            var reviewService = new Mock<IReviewService>();
            var newsService = new Mock<INewsService>();
            var cashService = new Mock<ICashService>();

            Game game = new Game("Far Cry 5", "Link", 100, 5) { Id = 1 };
            Player playerOne = new Player() { Id = 1 };
            Player playerTwo = new Player() { Id = 2 };
            playerOne.ReplenishMoney(800);                      
         
            GameService gameService = new GameService(deliveryService.Object, returnService.Object, giftService.Object, reviewService.Object, newsService.Object, cashService.Object);
            gameService.GiftGame(playerOne, playerTwo, game);
            
            giftService.Verify(x => x.GiftGame(playerTwo, game), Times.Once);            
            deliveryService.Verify(x => x.DeliverGame(game, playerTwo), Times.Once);
            Assert.AreEqual(700, playerOne.Balance);
            Assert.AreEqual(1, playerTwo.Games.Count);
        }

        [TestMethod]
        public void Write_OneReview_OneReviewAdd()
        {
            var deliveryService = new Mock<IDeliveryService>();
            var returnService = new Mock<IReturnService>();
            var giftService = new Mock<IGiftService>();
            var reviewService = new Mock<IReviewService>();
            var newsService = new Mock<INewsService>();
            var cashService = new Mock<ICashService>();

            Game game = new Game("Far Cry 5", "Link", 100, 5) { Id = 1 };
            Player playerOne = new Player() { Id = 1 };            
            playerOne.AddGame(game);
           
            GameService gameService = new GameService(deliveryService.Object, returnService.Object, giftService.Object, reviewService.Object, newsService.Object, cashService.Object);
            gameService.AddReview(playerOne, "Good Game", game);
            
            reviewService.Verify(x => x.AddReview(playerOne, "Good Game", game), Times.Once());
            Assert.AreEqual(1, game.Reviews.Count);
        }

        [TestMethod]
        public void Write_OneReviewToNotPurchasedGame_NoReviewAdd()
        {
            var deliveryService = new Mock<IDeliveryService>();
            var returnService = new Mock<IReturnService>();
            var giftService = new Mock<IGiftService>();
            var reviewService = new Mock<IReviewService>();
            var newsService = new Mock<INewsService>();
            var cashService = new Mock<ICashService>();

            Game game = new Game("Far Cry 5", "Link", 100, 5) { Id = 1 };
            Player playerOne = new Player() { Id = 1 };
            
            GameService gameService = new GameService(deliveryService.Object, returnService.Object, giftService.Object, reviewService.Object, newsService.Object, cashService.Object);
            try
            {
                gameService.AddReview(playerOne, "Good Game", game);
            }
            catch(Exception e)
            {
                Assert.AreEqual(e.Message, "Game not exists in player's Games");
            }

            reviewService.Verify(x => x.AddReview(playerOne, "Good Game", game), Times.Never());
            Assert.AreEqual(0, game.Reviews.Count);
        }

        [TestMethod]
        public void Buy_OneGameWithoutMoney_ZeroGameAdd()
        {
            var deliveryService = new Mock<IDeliveryService>();
            var returnService = new Mock<IReturnService>();
            var giftService = new Mock<IGiftService>();
            var reviewService = new Mock<IReviewService>();
            var newsService = new Mock<INewsService>();
            var cashService = new Mock<ICashService>();

            Game game = new Game("Far Cry 5", "Link", 500, 5) { Id = 1 };
            Player playerOne = new Player() { Id = 1 };
            playerOne.ReplenishMoney(100);
            GameService gameService = new GameService(deliveryService.Object, returnService.Object, giftService.Object, reviewService.Object, newsService.Object, cashService.Object);

            try
            {
                gameService.BuyGame(playerOne, game);
            }
            catch(Exception e)
            {
                Assert.AreEqual(e.Message, "Balance less then Game Price");
            }

            deliveryService.Verify(x => x.DeliverGame(game, playerOne), Times.Never);
            Assert.AreEqual(0, playerOne.Games.Count);
            Assert.AreEqual(playerOne.Balance, playerOne.Balance);
        }

        [TestMethod]
        public void Buy_OneGameAllreadyExists_ZeroGameAdd()
        {
            var deliveryService = new Mock<IDeliveryService>();
            var returnService = new Mock<IReturnService>();
            var giftService = new Mock<IGiftService>();
            var reviewService = new Mock<IReviewService>();
            var newsService = new Mock<INewsService>();
            var cashService = new Mock<ICashService>();

            Game game = new Game("Far Cry 5", "Link", 500, 5) { Id = 1 };
            Player playerOne = new Player() { Id = 1 };
            playerOne.ReplenishMoney(100);
            playerOne.AddGame(game);
            GameService gameService = new GameService(deliveryService.Object, returnService.Object, giftService.Object, reviewService.Object, newsService.Object, cashService.Object);
            try
            {
                gameService.BuyGame(playerOne, game);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Game already exist in User Account");
            }
            
            deliveryService.Verify(x => x.DeliverGame(game, playerOne), Times.Never);
            Assert.AreEqual(1, playerOne.Games.Count);
            Assert.AreEqual(playerOne.Balance, playerOne.Balance);
        }

        [TestMethod]
        public void Return_OneGame_OnaGameReturned()
        {
            var deliveryService = new Mock<IDeliveryService>();
            var returnService = new Mock<IReturnService>();
            var giftService = new Mock<IGiftService>();
            var reviewService = new Mock<IReviewService>();
            var newsService = new Mock<INewsService>();
            var cashService = new Mock<ICashService>();

            Game game = new Game("Far Cry 5", "Link", 500, 5) { Id = 1 };
            Player playerOne = new Player() { Id = 1 };
            playerOne.ReplenishMoney(600);
            
            GameService gameService = new GameService(deliveryService.Object, returnService.Object, giftService.Object, reviewService.Object, newsService.Object, cashService.Object);
            gameService.BuyGame(playerOne, game);
            Purchase purchase = new Purchase(playerOne, game);

            try
            {
                gameService.ReturnGame(purchase);
            }
            catch (Exception e)
            {
                Assert.AreNotEqual(e.Message, "Нельзя вернуть игру, которой у вас нет");
                Assert.AreNotEqual(e.Message, "Невозможно вернуть игру потому что гарантийный срок возврата истек");
            }

            returnService.Verify(x => x.ReturnGame(purchase), Times.Once);
            Assert.AreEqual(0, playerOne.Games.Count);
            Assert.AreEqual(600, playerOne.Balance);
        }

        [TestMethod]
        public void Return_OneWithEndOfWarrantDaysGame_OnaGameReturned()
        {
            var deliveryService = new Mock<IDeliveryService>();
            var returnService = new Mock<IReturnService>();
            var giftService = new Mock<IGiftService>();
            var reviewService = new Mock<IReviewService>();
            var newsService = new Mock<INewsService>();
            var cashService = new Mock<ICashService>();

            Game game = new Game("Far Cry 5", "Link", 500, 1) { Id = 1 };
            Player playerOne = new Player() { Id = 1 };
            playerOne.ReplenishMoney(600);

            GameService gameService = new GameService(deliveryService.Object, returnService.Object, giftService.Object, reviewService.Object, newsService.Object, cashService.Object);
            gameService.BuyGame(playerOne, game);
            Purchase purchase = new Purchase(playerOne, game)
            {
                CurrentTime = new DateTime(2018, 10, 5)
            };

            try
            {
                gameService.ReturnGame(purchase);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Невозможно вернуть игру, потому что гарантийный срок возврата истек");
            }

            returnService.Verify(x => x.ReturnGame(purchase), Times.Never);
            Assert.AreEqual(1, playerOne.Games.Count);
            Assert.AreEqual(100, playerOne.Balance);
        }

        [TestMethod]
        public void Gift_OneGameAllreadyExists_ZeroGameAdd()
        {
            var deliveryService = new Mock<IDeliveryService>();
            var returnService = new Mock<IReturnService>();
            var giftService = new Mock<IGiftService>();
            var reviewService = new Mock<IReviewService>();
            var newsService = new Mock<INewsService>();
            var cashService = new Mock<ICashService>();

            Game game = new Game("Far Cry 5", "Link", 100, 5) { Id = 1 };
            Player playerOne = new Player() { Id = 1 };
            Player playerTwo = new Player() { Id = 2 };
            playerOne.ReplenishMoney(800);
            playerTwo.AddGame(game);
            GameService gameService = new GameService(deliveryService.Object, returnService.Object, giftService.Object, reviewService.Object, newsService.Object, cashService.Object);
            try
            {
                gameService.GiftGame(playerOne, playerTwo, game);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Game already exist in User Account");
            }

            giftService.Verify(x => x.GiftGame(playerTwo, game), Times.Never);
            deliveryService.Verify(x => x.DeliverGame(game, playerTwo), Times.Never);
            Assert.AreEqual(800, playerOne.Balance);
            Assert.AreEqual(1, playerTwo.Games.Count);
        }

        [TestMethod]
        public void Gift_OneGameWithoutEnoughMoney_ZeroGameAdd()
        {
            var deliveryService = new Mock<IDeliveryService>();
            var returnService = new Mock<IReturnService>();
            var giftService = new Mock<IGiftService>();
            var reviewService = new Mock<IReviewService>();
            var newsService = new Mock<INewsService>();
            var cashService = new Mock<ICashService>();

            Game game = new Game("Far Cry 5", "Link", 200, 5) { Id = 1 };
            Player playerOne = new Player() { Id = 1 };
            Player playerTwo = new Player() { Id = 2 };
            playerOne.ReplenishMoney(100);
            
            GameService gameService = new GameService(deliveryService.Object, returnService.Object, giftService.Object, reviewService.Object, newsService.Object, cashService.Object);
            try
            {
                gameService.GiftGame(playerOne, playerTwo, game);
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.Message, "Balance less then Game Price");
            }

            giftService.Verify(x => x.GiftGame(playerTwo, game), Times.Never);
            deliveryService.Verify(x => x.DeliverGame(game, playerTwo), Times.Never);
            Assert.AreEqual(100, playerOne.Balance);
            Assert.AreEqual(0, playerTwo.Games.Count);
        }

        [TestMethod]
        public void Write_OneCompanyNews_OneNewsAdd()
        {
            var deliveryService = new Mock<IDeliveryService>();
            var returnService = new Mock<IReturnService>();
            var giftService = new Mock<IGiftService>();
            var reviewService = new Mock<IReviewService>();
            var newsService = new Mock<INewsService>();
            var cashService = new Mock<ICashService>();
            
            GameService gameService = new GameService(deliveryService.Object, returnService.Object, giftService.Object, reviewService.Object, newsService.Object, cashService.Object);
            Game game = new Game("Far Cry 5", "Link", 100, 5) { Id = 1 };
            GameCompany company = new GameCompany("Valve");
            company.Games.Add(game);
           
            gameService.AddNews(company, "New Game", game);
            newsService.Verify(x => x.AddNews(company, "New Game", game), Times.Once);
            Assert.AreEqual(1, game.News.Count);
        }

        [TestMethod]
        public void Write_OneCompanyNewsToNotSelfGame_NoNewsAdd()
        {
            var deliveryService = new Mock<IDeliveryService>();
            var returnService = new Mock<IReturnService>();
            var giftService = new Mock<IGiftService>();
            var reviewService = new Mock<IReviewService>();
            var newsService = new Mock<INewsService>();
            var cashService = new Mock<ICashService>();

            GameService gameService = new GameService(deliveryService.Object, returnService.Object, giftService.Object, reviewService.Object, newsService.Object, cashService.Object);
            Game game = new Game("Far Cry 5", "Link", 100, 5) { Id = 1 };
            GameCompany company = new GameCompany("Valve");
            
            try
            {
                gameService.AddNews(company, "New Game", game);
            }
            catch(Exception e)
            {
                Assert.AreEqual(e.Message, "Игровая компания не может добавить новость к чужой игре");
            }
            
            newsService.Verify(x => x.AddNews(company, "New Game", game), Times.Never);
            Assert.AreEqual(0, game.News.Count);
        }

        [TestMethod]
        public void Replenish_SomeMoney_SomeSumAdd()
        {
            var deliveryService = new Mock<IDeliveryService>();
            var returnService = new Mock<IReturnService>();
            var giftService = new Mock<IGiftService>();
            var reviewService = new Mock<IReviewService>();
            var newsService = new Mock<INewsService>();
            var cashService = new Mock<ICashService>();
            int sum = 100;

            GameService gameService = new GameService(deliveryService.Object, returnService.Object, giftService.Object, reviewService.Object, newsService.Object, cashService.Object);
            Player player = new Player() { Id = 1 };
            gameService.ReplenishMoney(player, sum);

            cashService.Verify(x => x.ReplenishMoney(player, sum), Times.Once);
            Assert.AreEqual(sum, player.Balance);
        }

        [TestMethod]
        public void Withdraw_SomeMoney_MinusSomeMoneyFromPlayerBalance()
        {
            var deliveryService = new Mock<IDeliveryService>();
            var returnService = new Mock<IReturnService>();
            var giftService = new Mock<IGiftService>();
            var reviewService = new Mock<IReviewService>();
            var newsService = new Mock<INewsService>();
            var cashService = new Mock<ICashService>();
            int withdrawSum = 100;
            
            GameService gameService = new GameService(deliveryService.Object, returnService.Object, giftService.Object, reviewService.Object, newsService.Object, cashService.Object);
            Player player = new Player() { Id = 1 };
            player.ReplenishMoney(200);
            decimal balanceBefore = player.Balance;
            gameService.WithdrawMoney(player, withdrawSum);

            cashService.Verify(x => x.WithdrawMoney(player, withdrawSum), Times.Once);
            Assert.AreEqual(balanceBefore - withdrawSum, player.Balance);
        }

        [TestMethod]
        public void Withdraw_MoreMoneyThenExists_NotMinusSomeMoneyFromPlayerBalance()
        {
            var deliveryService = new Mock<IDeliveryService>();
            var returnService = new Mock<IReturnService>();
            var giftService = new Mock<IGiftService>();
            var reviewService = new Mock<IReviewService>();
            var newsService = new Mock<INewsService>();
            var cashService = new Mock<ICashService>();
            int withdrawSum = 200;

            GameService gameService = new GameService(deliveryService.Object, returnService.Object, giftService.Object, reviewService.Object, newsService.Object, cashService.Object);
            Player player = new Player() { Id = 1 };
            player.ReplenishMoney(100);
            decimal balanceBefore = player.Balance;

            try
            {
                gameService.WithdrawMoney(player, withdrawSum);
            }
            catch(Exception e)
            {
                Assert.AreEqual(e.Message, "Сумма вывода денег превышает текущий баланс");
            }

            cashService.Verify(x => x.WithdrawMoney(player, withdrawSum), Times.Never);
            Assert.AreEqual(balanceBefore, player.Balance);
        }
    }
}
